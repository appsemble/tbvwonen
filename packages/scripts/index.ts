import yargs, { type CommandModule } from 'yargs';

import * as seedAccount from './commands/seed-account.js';

yargs()
  .command(seedAccount as unknown as CommandModule)
  .demandCommand(1)
  .parse(process.argv.slice(2));
