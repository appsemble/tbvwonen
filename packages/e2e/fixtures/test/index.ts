import { test as base, expect } from '@playwright/test';

const {
  BOT_ACCOUNT_EMAIL = 'bot@appsemble.com',
  BOT_ACCOUNT_PASSWORD = 'test',
  ORGANIZATION = 'appsemble',
} = process.env;

interface Fixtures {
  /**
   * Visit an app.
   *
   * Uses to values from the Appsemble class.
   *
   * @param appPath The path of the app to visit.
   */
  visitApp: (appPath: string) => Promise<void>;

  /**
   * Set the role of the user.
   *
   * @param app The app to set the role for.
   * @param role The role to set.
   *
   *   Note that this requires the user to be logged in to the studio.
   */
  changeRole: (app: string, role: string) => Promise<void>;

  /**
   * Set the role of the user in a team.
   *
   * @param app The app to set the role for.
   * @param team The team to set the role for.
   * @param role The role to set.
   */
  changeTeamRole: (app: string, team: string, role: 'manager' | 'member') => Promise<void>;

  /**
   * Login to the Appsemble studio.
   */
  studioLogin: () => Promise<void>;

  /**
   * Perform a logout in Appsemble Studio.
   */
  studioLogout: () => Promise<void>;

  /**
   * Login to an Appsemble app.
   *
   * Uses to values from the Appsemble class.
   */
  loginApp: (role: string) => Promise<void>;
}

export const test = base.extend<Fixtures>({
  async visitApp({ baseURL, page }, use) {
    await use(async (appPath: string) => {
      await page.goto(
        `http://${appPath}.${ORGANIZATION}.${new URL(baseURL ?? 'http://localhost:9999').host}`,
      );
    });
  },

  async studioLogin({ page }, use) {
    await use(async () => {
      const queryParams = new URLSearchParams({ redirect: '/en/apps' });
      await page.goto(`/en/login?${String(queryParams)}`);

      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

      await page.getByTestId('email').fill(BOT_ACCOUNT_EMAIL);
      await page.getByTestId('password').fill(BOT_ACCOUNT_PASSWORD);
      await page.getByTestId('login').click();
      // eslint-disable-next-line playwright/no-standalone-expect
      await expect(page).toHaveURL('/en/apps');
    });
  },

  async studioLogout({ page }, use) {
    await use(async () => {
      await page
        .locator(
          'div.navbar-item.has-dropdown.is-right > button.navbar-link:has(img[alt="Profile Picture"])',
        )
        .click();
      await page.getByRole('button', { name: 'Logout' }).click();
    });
  },

  async changeRole({ page }, use) {
    await use(async (app: string, role: string) => {
      const redirect = page.url();
      await page.goto(`/en/organizations/${ORGANIZATION}`);
      await page.click(`text=${app}`);
      await page.goto(`/en/apps/${page.url().split('/').pop()}/users`);
      const select = page.locator('tr', { hasText: 'It’s you!' }).locator('select[class=""]');
      await select.selectOption(role);
      await page.goto(redirect);
    });
  },

  async changeTeamRole({ page }, use) {
    await use(async (app: string, team: string, role: 'manager' | 'member') => {
      const redirect = page.url();
      await page.goto(`/en/organizations/${ORGANIZATION}`);
      await page.click(`text=${app.replaceAll('-', ' ')}`);
      const appId = page.url().split('/').pop();
      await page.goto(`/en/apps/${appId}/teams`);
      await page.click(`text=${team}`);
      await page.getByRole('row', { name: 'Appsemble Bot' }).locator('#role').selectOption(role);
      await page.goto(redirect);
    });
  },

  // We will be testing only the demo app.
  async loginApp({ page }, use) {
    await use(async (role: string) => {
      await page.waitForLoadState('domcontentloaded');
      await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

      const btn = page.getByTestId('create-account');
      if (await btn.isVisible()) {
        await page.getByTestId('app-role').selectOption(role);
        await btn.click();
      }
    });
  },
});
