name: Better Inside
defaultPage: Home
defaultLanguage: en
description: Maak meldingen over je woning.

theme:
  primaryColor: '#004C99'
  linkColor: '#009EE0'
  infoColor: '#97BE0D'
  dangerColor: '#DB001B'

layout:
  settings: navigation

security:
  default:
    role: Tenant
    policy: everyone

  roles:
    Staff:
      description: Role that can make livability reports.
      permissions:
        - $resource:all:create
    Tenant:
      description: General role for tenants.
      permissions:
        - $resource:repairRequest:create
  guest:
    permissions:
      - $resource:repairRequest:create

resources:
  repairRequest:
    # Requests will be purged 2 days after storing.
    expires: 2d
    schema:
      type: object
      additionalProperties: true
      required:
        - name
        - description
        - email
        - phone
        - street
        - postalCode
        - streetNumber
      properties:
        description:
          type: string
        photos:
          type: array
          items:
            type: string
            format: binary
        name:
          type: string
        email:
          type: string
          format: email
        postalCode:
          type: string
        streetNumber:
          type: string
        street:
          type: string
        phone:
          type: string
        contact:
          enum:
            - email
            - phone
        time:
          enum:
            - morning
            - afternoon
            - noPreference

  livabilityReport:
    expires: 2d
    schema:
      type: object
      additionalProperties: true
      required:
        - name
        - description
        - email
        - phone
        - street
        - postalCode
        - streetNumber
        - type
      properties:
        type:
          enum:
            - Burenruzie
            - Drugsoverlast
            - Eenzaamheid
            - Huiselijk geweld
            - Hennepkwekerij
            - Vervuiling/verzamelwoede
            - Overlast in algemene ruimte
            - Overig
        description:
          type: string
        photos:
          type: array
          items:
            type: string
            format: binary
        postalCode:
          type: string
        streetNumber:
          type: string
        street:
          type: string
        locationDescription:
          type: string
        name:
          type: string
        email:
          type: string
          format: email
        phone:
          type: string

anchors:
  - &appsemble-version 0.29.1
  - &required
    required: true
    errorMessage:
      translate: required
  - &postcode-regex-requirements
    regex: '^\s*[1-9][0-9]{3}[\s]?[A-Za-z]{2}\s*$'
    errorMessage: ' '
  - &postcode-regex-restrictions
    regex: '^(?!9999)\d{4}\s?[A-Z]{2}$'
    errorMessage:
      translate: notServiced
      # Services not provided in areas having pin code starting from 9999.
      # This has no practical value currently, just to show you're able to specify a permit list for the area your app services.
      # For example, this regex well allow service only in postcodes "5056 XX" and "5057 XX"
      # - regex: '^\s*(5056|5057|)[\s]?[A-Za-z]{2}\s*$'

pages:
  - name: Home
    icon: home
    theme:
      dangerColor: '#FEFEFE'
    blocks:
      - type: markdown
        layout: static
        version: *appsemble-version
        parameters:
          content:
            translate: intro
      - type: button-list
        version: *appsemble-version
        parameters:
          buttons:
            - label:
                translate: repairRequest
              color: primary
              onClick: onClick
        actions:
          onClick:
            type: link
            to: Reparatieverzoek
      - type: button-list
        version: *appsemble-version
        roles:
          - Staff
        parameters:
          buttons:
            - label:
                translate: livabilityReport
              color: info
              onClick: onStaffClick
        actions:
          onStaffClick:
            type: link
            to: Leefbaarheidsmelding
  - name: Reparatieverzoek
    type: flow
    hideNavTitle: true
    actions:
      onFlowFinish:
        type: resource.create
        resource: repairRequest
        onSuccess:
          type: email
          to: reparatielijn@tbvwonen.nl
          subject:
            translate: repairStaffEmailSubject
          body:
            string.format:
              messageId: repairStaffEmail
              values:
                id: { prop: id }
                type: { prop: type }
                description: { prop: description }
                name: { prop: name }
                email: { prop: email }
                phone: { prop: phone }
                street: { prop: street }
                streetNumber: { prop: streetNumber }
                postalCode: { prop: postalCode }
                contact: { prop: contact }
                time: { prop: time }
          attachments:
            - prop: photos
          onSuccess:
            type: email
            to:
              string.format:
                template: '{name} <{email}>'
                values:
                  name: { prop: name }
                  email: { prop: email }
            subject:
              translate: repairRequestEmailSubject
            body:
              string.format:
                messageId: repairRequestEmail
                values:
                  id: { prop: id }
                  type: { prop: type }
                  description: { prop: description }
                  name: { prop: name }
                  email: { prop: email }
                  phone: { prop: phone }
                  street: { prop: street }
                  streetNumber: { prop: streetNumber }
                  postalCode: { prop: postalCode }
                  contact: { prop: contact }
            attachments: { prop: photos }
            onSuccess:
              type: link
              to: Succes
    steps:
      - name: Stap 1 - Informatie over defect
        blocks:
          - type: markdown
            layout: static
            version: *appsemble-version
            parameters:
              content:
                translate: repairRequestDescription
          - type: form
            version: *appsemble-version
            header:
              translate: repairStep1
            parameters:
              fields:
                - name: description
                  type: string
                  multiline: true
                  label:
                    translate: descriptionProblem
                  requirements:
                    - *required
                  placeholder:
                    translate: descriptionProblemPlaceholder
                - name: photos
                  type: file
                  label:
                    translate: photos
                  maxWidth: 1920
                  maxHeight: 1920
                  quality: 80
                  repeated: true
                  requirements:
                    - accept:
                        - image/jpeg
                      errorMessage:
                        translate: invalidType
                    - maxLength: 4
                      errorMessage:
                        translate: fileLimit
            actions:
              onSubmit:
                type: flow.next
      - name: Stap 2 - Contactgegevens
        blocks:
          - type: form
            version: *appsemble-version
            header:
              translate: repairStep2
            actions:
              onSubmit:
                type: flow.next
              onPrevious:
                type: flow.back
              validateAddress:
                type: request
                url: https://api.pro6pp.nl/v2/autocomplete/nl
                proxy: false
                query:
                  object.from:
                    postalCode:
                      prop: postalCode
                    streetNumber:
                      prop: streetNumber
                remapBefore:
                  object.from:
                    postalCode:
                      - prop: postalCode
                      - string.replace:
                          '\s+': ''
                    streetNumber:
                      prop: streetNumber
                onSuccess:
                  type: noop
                  remapBefore:
                    object.from:
                      street:
                        prop: street
                onError:
                  type: static
                  value:
                    street: ''
            parameters:
              requirements:
                - isValid: ['streetNumber', 'postalCode']
                  action: validateAddress
                  errorMessage:
                    translate: invalidAddress
              previous: true
              fields:
                - name: name
                  type: string
                  label:
                    translate: name
                  placeholder:
                    translate: namePlaceholder
                  requirements:
                    - *required
                - name: postalCode
                  type: string
                  label:
                    translate: postalCode
                  placeholder: 1234 AB
                  requirements:
                    - *required
                    - *postcode-regex-requirements
                    - *postcode-regex-restrictions
                - name: streetNumber
                  type: string
                  label:
                    translate: houseNumber
                  placeholder: '123'
                  requirements:
                    - *required
                - name: street
                  type: string
                  label:
                    translate: street
                  readOnly: true
                  tag:
                    translate: automatic
                  requirements:
                    - *required
                - name: email
                  type: string
                  format: email
                  label:
                    translate: emailAddress
                  requirements:
                    - *required
                - name: phone
                  type: string
                  label:
                    translate: phoneNumber
                  requirements:
                    - *required
      - name: 3. Contactwijze
        blocks:
          - type: markdown
            layout: static
            version: *appsemble-version
            parameters:
              content:
                translate: repairStep3
          - type: form
            version: *appsemble-version
            parameters:
              previous: true
              fields:
                - name: contact
                  type: radio
                  label:
                    translate: contact
                  requirements:
                    - *required
                  options:
                    - value: phone
                      label:
                        translate: phone
                    - value: email
                      label:
                        translate: email
                - name: time
                  type: radio
                  requirements:
                    - *required
                  label:
                    translate: time
                  options:
                    - value: morning
                      label:
                        translate: morning
                    - value: afternoon
                      label:
                        translate: afternoon
                    - value: noPreference
                      label:
                        translate: noPreference
            actions:
              onSubmit:
                type: flow.finish
              onPrevious:
                type: flow.back
  - name: Leefbaarheidsmelding
    roles:
      - Staff
    theme:
      primaryColor: '#97BE0D'
    hideNavTitle: true
    type: flow
    actions:
      onFlowFinish:
        type: resource.create
        resource: livabilityReport
        onSuccess:
          type: email
          to: info@tbvwonen.nl
          subject: Nieuwe leefbaarheidsmelding
          body:
            string.format:
              messageId: livabilityEmail
              values:
                id: { prop: id }
                type: { prop: type }
                description: { prop: description }
                name: { prop: name }
                email: { prop: email }
                phone: { prop: phone }
                street: { prop: street }
                streetNumber: { prop: streetNumber }
                postalCode: { prop: postalCode }
                locationDescription: { prop: locationDescription }
          attachments: { prop: photos }
          onSuccess:
            type: link
            to: Succes Leefbaarheidsmelding
    steps:
      - name: Stap 1 - Informatie over melding
        blocks:
          - type: markdown
            layout: static
            version: *appsemble-version
            parameters:
              content:
                translate: livabilityDescription
          - type: form
            version: *appsemble-version
            header:
              translate: livabilityStep1
            parameters:
              fields:
                - name: type
                  type: enum
                  placeholder:
                    translate: type
                  label:
                    translate: type
                  requirements:
                    - *required
                  enum:
                    - value: Burenruzie
                      label:
                        translate: neighborConflict
                    - value: Drugsoverlast
                      label:
                        translate: drugsNuisance
                    - value: Eenzaamheid
                      label:
                        translate: loneliness
                    - value: Huiselijk geweld
                      label:
                        translate: domesticViolence
                    - value: Hennepkwekerij
                      label:
                        translate: hempFarm
                    - value: Vervuiling/verzamelwoede
                      label:
                        translate: pollution
                    - value: Overlast in algemene ruimte
                      label:
                        translate: generalNuisance
                    - value: Overig
                      label:
                        translate: other
                - name: description
                  type: string
                  multiline: true
                  label:
                    translate: descriptionIssue
                  requirements:
                    - *required
                  placeholder:
                    translate: descriptionProblemPlaceholder
                - name: photos
                  type: file
                  label:
                    translate: photos
                  maxWidth: 1920
                  maxHeight: 1920
                  quality: 80
                  repeated: true
                  requirements:
                    - accept: [image/jpeg]
                      errorMessage:
                        translate: invalidType
                    - maxLength: 4
                      errorMessage:
                        translate: fileLimit
            actions:
              onSubmit:
                type: flow.next
      - name: Stap 2 - Locatie
        blocks:
          - type: form
            version: *appsemble-version
            header:
              translate: livabilityStep2
            actions:
              onSubmit:
                type: flow.next
              onPrevious:
                type: flow.back
              validateAddress:
                proxy: false
                type: request
                url: https://api.pro6pp.nl/v2/autocomplete/nl
                query:
                  object.from:
                    postalCode:
                      prop: postalCode
                    streetNumber:
                      prop: streetNumber
                remapBefore:
                  object.from:
                    postalCode:
                      - prop: postalCode
                      - string.replace:
                          '\s+': ''
                    streetNumber: { prop: streetNumber }
                onSuccess:
                  type: noop
                  remapBefore:
                    object.from:
                      street: { prop: street }
                onError:
                  type: static
                  value:
                    street: ''
            parameters:
              requirements:
                - isValid: ['streetNumber', 'postalCode']
                  action: validateAddress
                  errorMessage:
                    translate: invalidAddress
              previous: true
              fields:
                - name: postalCode
                  type: string
                  label:
                    translate: postalCode
                  placeholder: 1234 AB
                  requirements:
                    - *required
                    - *postcode-regex-requirements
                    - *postcode-regex-restrictions
                - name: streetNumber
                  type: string
                  label:
                    translate: houseNumber
                  placeholder: '123'
                  requirements:
                    - *required
                - name: street
                  type: string
                  label:
                    translate: street
                  readOnly: true
                  tag:
                    translate: automatic
                  requirements:
                    - *required
                - name: locationDescription
                  label:
                    translate: locationDescription
                  type: string
                  multiline: true
                  placeholder:
                    translate: locationDescriptionDescription
      - name: 3. Contactgegevens
        blocks:
          - type: form
            version: *appsemble-version
            header:
              translate: livabilityStep3
            parameters:
              previous: true
              fields:
                - name: name
                  type: string
                  label:
                    translate: name
                  placeholder:
                    translate: namePlaceholder
                  requirements:
                    - *required
                - name: email
                  type: string
                  format: email
                  label:
                    translate: emailAddress
                  requirements:
                    - *required
                - name: phone
                  type: string
                  label:
                    translate: phoneNumber
                  requirements:
                    - *required
            actions:
              onSubmit:
                type: flow.finish
              onPrevious:
                type: flow.back

  - name: Succes
    hideNavTitle: true
    blocks:
      - type: markdown
        layout: static
        version: *appsemble-version
        parameters:
          content:
            translate: success
      - type: button-list
        version: *appsemble-version
        parameters:
          buttons:
            - label:
                translate: returnHome
              outlined: true
              color: primary
              onClick: onClick
        actions:
          onClick:
            type: link
            to: Home
  - name: Succes Leefbaarheidsmelding
    hideNavTitle: true
    roles:
      - Staff
    theme:
      primaryColor: '#97BE0D'
    blocks:
      - type: markdown
        layout: static
        version: *appsemble-version
        parameters:
          content:
            translate: successLivability
      - type: button-list
        version: *appsemble-version
        parameters:
          buttons:
            - label:
                translate: returnHome
              outlined: true
              color: primary
              onClick: onClick
        actions:
          onClick:
            type: link
            to: Home
